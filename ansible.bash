#!/bin/bash

if ! ansible --version >& /dev/null; then
  sudo apt install ansible -y
fi
ansible-playbook site.yml -i dev --ask-become-pass

